using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;

public class CreateRoomMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] 
    private Text roomName;

    private RoomsCanvases _roomCanvas;

    public void FirstInitialize(RoomsCanvases canvases)
    {
        _roomCanvas = canvases;
    }
    
    public void OnClick_CreateRoom()
    {
        var options = new RoomOptions
        {
            MaxPlayers = 4
        };
        PhotonNetwork.JoinOrCreateRoom(roomName.text, options, TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Room created");
        //_roomCanvas.CurrentRoomCanvas.Show();
    }

    public override void OnCreatedRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room created failed");
    }

    public override void OnLeftRoom()
    {
        Debug.Log("Leave room successfully");

    }
    
    public override void OnJoinedLobby()
    {
        print("JoinedLobby");
    }
}