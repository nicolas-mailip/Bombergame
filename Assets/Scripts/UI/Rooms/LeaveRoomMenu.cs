using Photon.Pun;
using UnityEngine;

public class LeaveRoomMenu : MonoBehaviour {


    private RoomsCanvases roomCanvas;

    public void FirstInitialize(RoomsCanvases canvases) => roomCanvas = canvases;

    public void OnClick_LeaveRoom() 
    {
        PhotonNetwork.LeaveRoom(true);
        roomCanvas.CurrentRoomCanvas.Hide();
    }

}