using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class RoomsCanvases : MonoBehaviour
{
  [SerializeField]
  private CreateOrJoinRoomCanvas createOrJoinRoomCanvas;

  public CreateOrJoinRoomCanvas CreateOrJoinRoomCanvas => createOrJoinRoomCanvas;

  [SerializeField]
  private CurrentRoomCanvas currentRoomCanvas;

  public CurrentRoomCanvas CurrentRoomCanvas => currentRoomCanvas;

  private void Awake()
  { 
    FirstInitialize();
  }

  private void FirstInitialize()
  {
    CreateOrJoinRoomCanvas.FirstInitialize(this);
    CurrentRoomCanvas.FirstInitialize(this);
  }
}
