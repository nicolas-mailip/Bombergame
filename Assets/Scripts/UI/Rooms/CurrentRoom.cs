using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class CurrentRoom : MonoBehaviourPunCallbacks
{
    public void OnClick_LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
    
}
