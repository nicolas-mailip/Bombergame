using System.Collections;
using System.Collections.Generic;

using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviour
{
    [SerializeField]
    private Text text;

    public Photon.Realtime.Player Player { get; private set; }
    
    public void SetPlayerInfo(Photon.Realtime.Player player)
    {
        Player = player;
        text.text = player.NickName;
    }
}
