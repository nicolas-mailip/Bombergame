using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon.Realtime;

public class PlayerListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Transform content;
    [SerializeField]
    private PlayerListing playerListing;

    private readonly List<PlayerListing> listings = new();
    private RoomsCanvases roomsCanvas;

    public void FirstInitialize(RoomsCanvases canvases) => roomsCanvas = canvases;

    private void Awake()
    {
        GetCurrentRoomPlayers();
    }

    private void GetCurrentRoomPlayers()
    {   
        foreach (KeyValuePair<int, Photon.Realtime.Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        }
    }

    private void AddPlayerListing(Photon.Realtime.Player newPlayer)
    {
        PlayerListing listing = (PlayerListing)Instantiate(playerListing, content);

        if (listing != null)
        {
            listing.SetPlayerInfo(newPlayer);
            listings.Add(listing);
        }
    }
    
    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        content.DestroyChildren();
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        AddPlayerListing(newPlayer);
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        var index = listings.FindIndex(x => x.Player == otherPlayer);
        if (index != -1)
        {
            Destroy(listings[index].gameObject);
            listings.RemoveAt(index);
        }
    }
}
