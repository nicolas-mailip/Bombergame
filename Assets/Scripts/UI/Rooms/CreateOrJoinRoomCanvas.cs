using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CreateOrJoinRoomCanvas : MonoBehaviour
{
    [SerializeField]
    private CreateRoomMenu createRoomMenu;

    [SerializeField]
    private RoomListingsMenu roomListingsMenu;
    
    private RoomsCanvases _roomCanvas;

    public void FirstInitialize(RoomsCanvases canvases)
    {
        _roomCanvas = canvases;
        createRoomMenu.FirstInitialize(canvases);
        roomListingsMenu.FirstInitialize(canvases);
    }
}
