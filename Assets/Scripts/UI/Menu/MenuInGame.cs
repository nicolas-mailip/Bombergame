using System.Collections;
using System.Collections.Generic;

using Photon.Pun;
using UnityEngine;

public class MenuInGame : MonoBehaviour {

    [SerializeField]
    private LeaveMenuInGame leaveMenuInGame;

    public LeaveMenuInGame LeaveMenuInGame { get => leaveMenuInGame; set => leaveMenuInGame = value; }

    private void Start() {
        FirstInitialize();    
    }

    public void FirstInitialize()
    {
        LeaveMenuInGame.FirstInitialize(this);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}