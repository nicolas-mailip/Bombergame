
using System;
using System.Collections;
using System.Collections.Generic;

using Photon.Pun;
using UnityEngine;

public class LeaveMenuInGame : MonoBehaviourPunCallbacks
{
    private MenuInGame menuInGameCanvas;

    public void FirstInitialize(MenuInGame menuInGame)
    {
        menuInGameCanvas = menuInGame;
    }

    public void OnClick_LeaveMenu()
    {
        menuInGameCanvas.Hide();
    }
}
