﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public Text fireNumber;
    public Text bombNumber;
    public Text speedNumber;

    void Start()
    {
    }
    void Update()
    {
    }

    public void setFire(string explosions)
    {
        this.fireNumber.text = explosions;
    }

    public void setBomb(string bomb)
    {
        this.bombNumber.text = bomb;
    }

    public void setSpeed(string speed)
    {
        this.speedNumber.text = speed;
    }
}