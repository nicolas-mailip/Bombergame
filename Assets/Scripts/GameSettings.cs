using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "Manager/GameSettings")]
public class GameSettings : ScriptableObject
{
   [FormerlySerializedAs("_gameVersion")]
   [SerializeField]
   private string gameVersion = "0.0.0";
   
   public string GameVersion => gameVersion;
   
   [FormerlySerializedAs("_nickName")]
   [SerializeField]
   private string nickName = "Bomberman";

   public string NickName
   {
      get
      {
         var value = Random.Range(0, 9999);
         return nickName + value.ToString();
      }
   }
}
