using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;


public class	Bomb : MonoBehaviourPunCallbacks
{

	public GameObject explosionPrefab;
	public LayerMask levelMask;
	private bool exploded = false;
	private int explosions;
	List<Collider> deleteObjects = new List<Collider>();


	// Start is called before the first frame update
	void 	Start()
	{
		Invoke("Explode", 4f);
	}

	// Update is called once per frame
	void	Update()
	{

	}

	void	Explode()
	{
		PhotonNetwork.Instantiate(explosionPrefab.name, transform.position, Quaternion.identity);

		StartCoroutine(CreateExplosion(Vector3.forward));
		StartCoroutine(CreateExplosion(Vector3.right));
		StartCoroutine(CreateExplosion(Vector3.back));
		StartCoroutine(CreateExplosion(Vector3.left));

		GetComponent<MeshRenderer>().enabled = false;
		exploded = true;
		transform.Find("Collider").gameObject.SetActive(false);


		photonView.RPC(nameof(RPC_DestroyForAll), RpcTarget.AllBuffered);
	}

	[PunRPC]
	protected void RPC_DestroyForAll()
	{
		Destroy(gameObject, .3f);
	}

	private	IEnumerator CreateExplosion(Vector3 direction)
	{
		for (int i = 1; i < explosions; i++)
		{
			RaycastHit hit;

			Physics.Raycast(transform.position + new Vector3(0, .5f, 0), direction, out hit, i, levelMask);


			if (!hit.collider)
			{
				PhotonNetwork.Instantiate(explosionPrefab.name, transform.position + (i * direction), explosionPrefab.transform.rotation);
			}
			else
			{
				if (hit.collider.tag == "Box")
				{
					PhotonView box = hit.collider.GetComponent<PhotonView>();
					hit.collider.gameObject.GetComponent<Box>().DestroyAndSpawnNewOne();
					/*hit.collider.gameObject.GetComponent<Box>().DestroyForAll();*/
			        /*test(hit);*/


				/* TransferOwnership(PhotonNetwork.LocalPlayer.ActorNumber);*/

					/*hit.collider.gameObject.GetComponent<Box>().DestroyAndSpawnNewOne();*/
				}

				break;
			}

			yield return new WaitForSeconds(.05f);
		}

	}


	public void 	SetExplosions(int setNumber)
	{
		this.explosions = setNumber;
	}

	public void		OnTriggerEnter(Collider other)
	{
		if (!exploded && other.CompareTag("Explosion"))
		{
			CancelInvoke("Explode");
			Explode();
		}
		if (other.CompareTag("Box"))
		{
			Debug.Log("PAn");
		}

	}

}
