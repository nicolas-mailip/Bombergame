﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

using UnityEngine.UI;

using Photon.Realtime;
using Photon.Pun;

public class			Box : MonoBehaviourPunCallbacks
{
    public GameObject	destroyedBoxPrefab;
    public GameObject[]	powerUpPrefab;
    

	public void		DestroyForAll()
	{
		if (PhotonNetwork.LocalPlayer.ActorNumber != photonView.sceneViewId)
		photonView.RPC(nameof(RPC_DestroyForAll), RpcTarget.AllBuffered);
	}

	[PunRPC]
	protected void	RPC_DestroyForAll()
	{
		Destroy(gameObject);
	}
	
	public void 	DestroyAndSpawnNewOne()
    {
		int			random	= Random.Range(0, 3);
        GameObject	box 	= PhotonNetwork.Instantiate(destroyedBoxPrefab.name,
                                                   gameObject.transform.position,
                                                   Quaternion.identity);

        photonView.RPC(nameof(RPC_DestroyForAll), RpcTarget.AllBuffered);
        
		if (Random.Range(0.0f, 100.0f) > 70.0f)
			{
	        	PhotonNetwork.Instantiate("Bonus/" + powerUpPrefab[random].name, 
					new Vector3(
                        gameObject.transform.position.x,
                        -0.8f,
                        gameObject.transform.position.z), Quaternion.identity);

				Debug.Log("Bonus/" + powerUpPrefab[random].transform.rotation.x);
			}
    }
}