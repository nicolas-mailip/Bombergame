﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

using UnityEngine.UI;

using Photon.Realtime;
using Photon.Pun;

public class Timer : MonoBehaviourPunCallbacks
{
    public double timeRemaining = 180;
    public Text timerPrint;
    
    void Start()
    {
        this.timerPrint.text = "10:00";
    }

    void DisplayTime(double timeToDisplay)
    {
        timeToDisplay += 1;

        double minutes = Mathf.Floor((float)timeToDisplay / 60);
        double seconds = Mathf.Floor((float)timeToDisplay % 60);
        
        timerPrint.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    void Update()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            DisplayTime(timeRemaining);
        }
        else
        {
        }
    }

}