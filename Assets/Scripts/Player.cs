﻿/*
 * Copyright (c) 2017 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;
using System.Collections;
using System;

using Photon.Realtime;
using Photon.Pun;

using UnityEngine.UI;


public class Player : MonoBehaviourPunCallbacks
{

        
        //Player parameters
        [Range(1, 2)] //Enables a nifty slider in the editor

        /*
         * Variable
         */
		public float	slowSpeed = 2f;
        public float	moveSpeed = 4f;
        public bool		canDropBombs = true;
        public bool		canMove = true;
        public bool		dead = false;
		public int 		explosions = 3;
        public int		bombsAvailable = 1;

        private float	dropCooldown = 4.0f;

        /*
         * GlobalStateManager
         */
        public GlobalStateManager globalManager;
		
        /*
         * Prefabs
         */
        [SerializeField]
        public GameObject bombPrefab;
        
        [SerializeField]
        private PhotonView view;


        /*
         * Cached components
         */
        private Rigidbody rigidBody;
        private Transform myTransform;
        private Animator animator;

		[SerializeField]
		public PlayerStats stats;

		public string nickName;
		void	Awake()
		{

		}


        void	Start()   
        {

            rigidBody = GetComponent<Rigidbody>();
            myTransform = transform; 
            animator = GetComponent<Animator>();
            animator.speed = moveSpeed;
            view = GetComponent<PhotonView>();
			
			stats = GameObject.Find("Stats").GetComponent<PlayerStats>();
			stats.setFire(this.explosions.ToString());
			stats.setBomb(this.bombsAvailable.ToString());
			stats.setSpeed(this.moveSpeed.ToString());
        }
        
        
        /*
         * Update is called once per frame
         */
        void	Update()
        {
            animator.SetBool("Walking", false);
            if (view.IsMine)
            {
                UpdateClient();
            }
        }

        /*
         * Input players
         */
        private void	UpdateClient()
        {
          
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, moveSpeed);
                transform.rotation = Quaternion.Euler(0, 0, 0);
                animator.SetBool("Walking", true);
            } else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.DownArrow))
            {
                rigidBody.velocity = new Vector3(-moveSpeed, rigidBody.velocity.y, rigidBody.velocity.z);
                transform.rotation = Quaternion.Euler(0, 270, 0);
                animator.SetBool("Walking", true);
            } else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.LeftArrow))
            {
                rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, -moveSpeed);
                transform.rotation = Quaternion.Euler(0, 180, 0);
                animator.SetBool("Walking", true);
            }else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                rigidBody.velocity = new Vector3(moveSpeed, rigidBody.velocity.y, rigidBody.velocity.z);
                transform.rotation = Quaternion.Euler(0, 90, 0);
                animator.SetBool("Walking", true);
            }
            
            if (canDropBombs && Input.GetKeyDown(KeyCode.Space))
            {
                if (this.bombsAvailable > 0)
                {
					this.bombsAvailable -= 1;
                    DropBomb();
                    StartCoroutine(StartCooldown());
                }

            }
        }

        public IEnumerator	StartCooldown()
        {
            float timeStamp = Time.time + dropCooldown;
            while (timeStamp >= Time.time)
            {
                yield return null;
            }
			this.bombsAvailable += 1;
        }

		/*
		* Drop bomb and set explosion
		*/
		
        private void 	DropBomb()
        {
            if (bombPrefab)
            {
               GameObject bomb = PhotonNetwork.Instantiate(bombPrefab.name,
                    new Vector3(Mathf.Round(transform.position.x), bombPrefab.transform.position.y,
                        Mathf.Round(transform.position.z)), bombPrefab.transform.rotation);
				 bomb.GetComponent<Bomb>().SetExplosions(this.explosions);
            }
        }
        

		private void	ApplyPowerUp(int powerUpType)
		{
			switch (powerUpType)
			{
				case 0:
					{
						this.explosions += 1;
						stats.setFire(this.explosions.ToString());
						break ;
					}
				case 1:
					{
						this.bombsAvailable += 1;
						stats.setBomb(this.bombsAvailable.ToString());
						break ;
					}
				case 2:
					{
						this.moveSpeed += 0.5f;
						this.slowSpeed += 0.5f;
						stats.setSpeed(this.moveSpeed.ToString());
		                animator.speed = moveSpeed;
						break ;
					}
			}
		} 

        public void     DestroyPlayer()
        {
            Destroy(gameObject);
        }
        
		/*
		* Check trigger when explosion and powerup happened
		*/
        public void 	OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Explosion"))
            {
                dead = true; // 1
                /*        globalManager.PlayerDied(playerNumber); // 2 */
                DestroyPlayer();  
            }
            if (other.CompareTag("PowerUp"))
            {
                PowerUp powerUp = other.gameObject.GetComponent<PowerUp>();
				this.ApplyPowerUp((int)powerUp.type);
                Destroy(other.gameObject);
            }
        }

		/*
		* check ground collide for slow in water block
		*/
		public void 	OnCollisionEnter(Collision hit)
		{
			 if (hit.gameObject.CompareTag("Water"))
				moveSpeed = slowSpeed;
             else
			    moveSpeed = slowSpeed * 2;
		}

 		public void OnCollisionStay (Collision target)
    	{
        	if (target.gameObject.tag == "Bomb")
        	{
            	Rigidbody rigidbody = target.collider.attachedRigidbody;

	           /* Vector3 forceDirection = target.gameObject.transform.position - myTransform.position;

    	        rigidbody.AddForceAtPosition(forceDirection, myTransform.position, ForceMode.Impulse); */
        	}
    	}

}