﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour
{
    public static bool ShouldSpawn(float chance)
    {
        return(Random.Range(0.0f, 100.0f) > (100.0f - chance));
    }
}