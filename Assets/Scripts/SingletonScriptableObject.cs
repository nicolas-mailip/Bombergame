using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonScriptableObject<T> : ScriptableObject where T : ScriptableObject
{
    private static T _instance = null;

    public static T Instance
    {
        get
        {
            if (_instance != null)
                return _instance;
            T[] results = Resources.FindObjectsOfTypeAll<T>();
            switch (results.Length)
            {
                case 0:
                    Debug.LogError("SingletonScriptableObject: results length is 0 of " + typeof(T).ToString());
                    return null;
                case > 1:
                    Debug.LogError("SingletonScriptableObject: results length is greater than 1 of " + typeof(T).ToString());
                    return null;
            }

            _instance = results[0];
            _instance.hideFlags = HideFlags.DontUnloadUnusedAsset;

            return _instance;
        }
    }
}
