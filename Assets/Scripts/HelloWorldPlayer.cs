using Unity.Netcode;
using UnityEngine;
namespace HelloWorld
{
    public class HelloWorldPlayer : NetworkBehaviour
    {
        public NetworkVariable<Vector3> Position = new NetworkVariable<Vector3>();
        private Rigidbody rigidBody;
        private Animator animator;
        private Transform myTransform;

    
        public override void OnNetworkSpawn()
        {
            rigidBody = GetComponent<Rigidbody>();
            myTransform = transform;

            animator = myTransform.Find("PlayerModel").GetComponent<Animator>();
            if (IsOwner)
            {
                Move();
            }
        }

        public void Move()
        {
            if (NetworkManager.Singleton.IsServer)
            {
                transform.position = new Vector3(1f, 0.5f, 1);
                Position.Value =  transform.position;
            }
            else
            {
                Update();
            }
        }

        void Update()
        {
            animator.SetBool("Walking", false);

            if (IsServer && IsOwner)
            {
                if (Input.GetKey(KeyCode.W))
                {
                    //Up movement
                    rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, 5f);
                    animator.SetBool("Walking", true);
                }

                if (Input.GetKey(KeyCode.A))
                {
                    //Left movement
                    rigidBody.velocity = new Vector3(-5f, rigidBody.velocity.y, rigidBody.velocity.z);
                    animator.SetBool("Walking", true);
                }

                if (Input.GetKey(KeyCode.S))
                {
                    //Down movement
                    rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, -5f);
                    animator.SetBool("Walking", true);
                }

                if (Input.GetKey(KeyCode.D))
                {
                    //Right movement
                    rigidBody.velocity = new Vector3(5f, rigidBody.velocity.y, rigidBody.velocity.z);
                    animator.SetBool("Walking", true);
                }
            }
           
        }
        
        [ServerRpc]
        void SubmitPositionRequestServerRpc(ServerRpcParams rpcParams = default)
        {
            Position.Value = GetRandomPositionOnPlane();
        }

        public void UpdateMove(Vector3 NewPosition)
        {
        }
        
        static Vector3 GetRandomPositionOnPlane()
        {
            return new Vector3(Random.Range(-3f, 3f), 1f, Random.Range(-3f, 3f));
        }

    }
}