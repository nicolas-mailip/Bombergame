﻿using System.Collections;
using UnityEngine;
using System;

using Photon.Realtime;
using Photon.Pun;

public class SpawnPlayer : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;

    public Transform[] players;

    void Start()
    {
        int numberInRoom;
        Photon.Realtime.Player[] playerList;
        
        numberInRoom = 0;
        playerList = PhotonNetwork.PlayerList;

        for (int i = 0; i < playerList.Length; i++)
        {
            Photon.Realtime.Player p = playerList[i];
            if (p != PhotonNetwork.LocalPlayer)
            {
                numberInRoom += 1;
            }
        }
        Debug.Log(playerList.Length);

        // Vector3 playerPosition = new Vector3(1f, 0.53f, 1f);
       
        PhotonNetwork.Instantiate(playerPrefab.name, players[numberInRoom].position, Quaternion.identity);
    }
}