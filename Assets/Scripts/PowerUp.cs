﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;
	                   
public class PowerUp : MonoBehaviourPunCallbacks
{
    public enum PowerUpType
    {
        Fire,
        Bomb,
        Speed,
        Kick
    }

    public PowerUpType type;

    void Start()
    {
    }
 
    
    private void OnTriggerEnter(Collider other)
    {
        PhotonNetwork.Destroy(gameObject);
    }
}